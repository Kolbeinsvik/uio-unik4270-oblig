# Oblig UNIK4270

Martin Kolbeinsvik (martikol)

## Part 1: Buffer overflow

### Q1) Explain shortly the difference of `identification`, `authentication` and `authorization`

**Identification**  
Claiming to be someone. E.g. entering a `username` or `email address`.

**Authentication**  
Proving to be who you identify as. E.g. the password (in combination with the username/email). The password is `something you know` that the system associates with that identity. It is possible to be authenticated several times, such as when you are asked to enter your password when using the `sudo` command.

Another option is possesion-based authentication where you authenticate with `something you have`, such as a OTP-token for BankID.

The last option is bio-based authentication where you authenticate with `something you are`, such as a thumb print.

**Authorization**  
Authorization takes place after identification and authentication, and it defines what you are allowed to do. E.g. As user `x` you have access to your own files but not the system files or `y`'s files. You can run these commands but not those, etc.


### Q2) Physical access

#### Q2a) Why would physical access to the machine be handy for rooting it?

Having physical access to the machine makes it easier for an attacker to rooting it as more options are available. Many of these options are there to make maintenance easier, and to make it easier for the user to regain legitimate access should he or she forget their password.

One way is through `LILO` (Linux Loader), a popular boot loader for Linux, where root-access can be granted in Runlevel 1 (single user mode). Steps can be made to harden the LILO to require a password.

If the drive on the machine is not encrypted an attacker can get root access by booting the machine from a removable media device, such as `SystemRescueCd`, and mounting the drive. The drive can also be removed from the machine and added as a drive to the attacker's machine, having much the same result.


#### Q2b) Give an example on what you could do in an attempt to access the locked room by use of social engineering.

* One could claim to have forgotten keys or access card (pretending to have legitimate access or pretending to be someone else).
* One could tail a person or group that is entering the room and follow them in.
* One could ask to be let in.


### Q3) Filling the `hosts` file with domain names, would make those domains inaccessible for the other users of the system. Would you consider this to be a `Confidentiality`, `Integrity` and/or `Availability` issue, and why/why not?

This could be an issue of `confidentiality`. Confidentiality is about managing who can access what so that information is not disclosed to unauthorized parties. By blocking access to those domains it limits access for all users, who might be seen as unauthorized.

This could be an issue of `availability`. Availability assures that information is available when needed. If a domain is blocked this cuts availability for all users, which can be seen as a `denial of service` attack.

This is not an issue of `integrity`. Integrity pertains to the integrity of the data, that the data is not modified by unauthorized parties. The domains are not modified, nor are the information on them changed.


### Q4) Buffer overflow

#### Q4a) What us a `NOP-sled`?

A `NOP` is an `no operation` instruction. It does nothing by itself, but allows a program to jump into a specific address, skip the NOP instructions and continue from the end of them. A `Nop-sled` is a sequence of NOP instructions designed to "pad" the memory area in cases where a desired position cannot be absolutely determined.


#### Q4b) Why are repeated return addresses (in combination with the NOP-sled) helpful when you deliver the shellcode?

#### Q4c) `blocker` was compiled with `-fno-stack-protector` and linked with `-z execstack`. This disabled two important security features, what do these do, and would you be able to run the exploit if the features had been enabled?

The `fno-stack-protector` flag disables the stack protector, which helps detect overflow attacks by attempting to detect tampering.

The `-z execstack` flag allows code in the stack to be executed.

I would not be able to successfully exploit the `bloker` program with `shellcode.py` if these features had been enabled.


#### Q4d) What does address space layout randomization (`randomize_va_space`) do, and would you be able to run the exploit if it had been enabled?

Disabling `randomize_va_space` makes address space consistent between runs and helps debugging as it makes bugs non-deterministic. If it was enabled the location memory location would have been different each time, and that would have made the attack fail as the `shellcode.py` uses a hard-coded address.

Shellcode-spraying attack could still work, as it is indifferent to memory layout. Attacks relying on relative addressing would still work.


#### Q4e) Do an Internet search and find an example of a program that is or has been vulnerable to a `buffer overflow`. Take note of interesting data e.g.: the url, the program name and what is does, vulnerability description etc.

**Remote Buffer Overflow in Sendmail**  
**Program name**: Sendmail Pro (all versions)  
**Url**: http://www.cert.org/historical/advisories/CA-2003-07.cfm  
**Impact**: Successful exploitation of this vulnerability may allow an attacker to gain the privileges of the sendmail daemon, typically root.  Even vulnerable sendmail servers on the interior of a given network may be at risk since the vulnerability is triggered from the contents of a malicious email message.


## Part 2: Password cracking

### Q5) Access to `passwd` and `shadow`

#### Q5a) Why could it be useful for an attacker to crack passwords on a system he already got root access to?

If the attacker could use someone else's account then actions made would be linked back to that account only.

#### Q5b) What does the `unshadow` command do?

The `unshadow` command combines the data from both the `passwd` file and `shadow` file. It lets `john` make more "informed" choices when cracking the passwords. Specifically (from the man pages), if it was not done, "the GECOS information wouldn't be used by the "single crack" mode, and also you wouldn't be  able  to use the '-shells' option."

### Q6) Singe crack mode
`$ john -single workfile`

#### Q6a) What are the names of the persons with the weakest passwords on the system, and what is so bad about these passwords?

The users with the weakest passwords are `linebo` and `Langbein`.

`linebo`'s password `Borgund` is her last name and can be found in the `passwd` file.  
`Langbein`'s password `langbein12` is the same as his username, only in lowercase and with a number appended to the end.


#### Q6b) Where does the information to crack these passwords come from, and what does the single crack mode do?

In single crack mode, the information to crack the passwords come from the login/GECOS information in combination with mangling rules, such as variations in casing and the addition of numbers.

### Q7) Wordlist mode

`$ john -wordlist=/usr/share/john/password.lst workfile -rules`

#### Q7a) What is this kind of password attack called (according to Gollmann)?

Dictionary attack.

#### Q7b) What is the difference between the wordlist with and without the rules?

Without the rules only the exact wording of the words in the wordlist are tried as passwords. With the rules the words are mutated according to the different rules and tried. E.g. `coffee` is `eeffoc` with lowercase and reversed ordering.

#### Q7c) Inspect the `Word List Rules` in the file `/etc/john/john.conf` • Which rule successfully cracked Dole's password? • Which rule cracked Doffen's password?

**Dole**  
Password: `cofee6`  
Rule: `# Lowercase pure alphabetic words and append a digit or simple punctuation`

**Doffen**  
Password: `eeffoc`  
Rule: `# Lowercase and reverse pure alphabetic words`


#### Q7d) What is salt and would use of salt prevent this attack? Why/why not?

Salt is a unique string that is prepended or appended to the password before it is hashed. It does not have to be a secret string and can be saved right next to the stored hash. Having a salted password means that rainbow tables cant be used to check the password as the hashed values would not match. Salting would not stop this attack from succeeding.


### Q8) Brute force attack

`$ john -incremental workfile`
```
[Incremental:skrue]
File = $JOHN/ascii.chr
MinLen = 1
MaxLen = 4
charCount = 24
```

#### Q8a) What is this kind of password attack called (according to Gollmann)?

Brute-force attack.


#### Q8b) Why is the password of Dolly stronger from the john point of view, even when it is only three characters long?

Dolly's password `gcc` is stronger because the it, or combinations of it, does not exist in the wordfile. It takes brute-force "guessing" to crack (it is not a strong password to brute-force).


#### Q8c) If you have one password of the max length of 4 and only consisting of the lowercase from `a-z` and numbers from `0-9`, how many combinations must you try at the max to crack the password?

The maximum number of attempts would be 36<sup>4</sup> (1,679,616).


#### Q8d) John can be configured to only search for passwords that satisfies the criteria given in the example above. By using the information from the status of john, calculate the maximum time it would take to crack this one password.

#### Q8e) Skrue has a password that satisfies the criteria given above. Name one of the reasons why it takes much longer time to crack this password when running john with default configurations that what your calculations showed. (hint: check the config file or/and keep pressing a button for status information to see what passwords john tries)

A more specific criteria limits the total amounts of attempts `john` have to try to find the password (as long as it matches the criteria).

#### Q8f) Could you think of a rule you could apply to the wordlist attack that would crack Skrue's password?

A rule that substitutes letters with numbers in accordance with `leet`-speak (1337 5p34k). E.g. `e` is `3`, `o` is `0`, etc.


#### Q8g) If you were to set a password policy for an enterprise, which rules would you enforce if:

##### Q8g I) you want to enforce the strongest password policy possible, and the human factor is not to be considered. How would this policy be?

Passwords should be long strings containing a random combination of letters, numbers and symbols. E.g. `@sDhQvkBz^U!tMw$stJ#D-@u#YSd7vH9^xAk$JCJA3aDzR*M9+@9_4ceXWKztAnjWd&By4*2wA=E7SBpg4qjGrAJPhgn@L6=gqa!Vr##b%dgT$A3grXrqKbu53PVDAWTKcpuFmMD-ACEPz4&3JX!=JxcZy$Q&jVtx2sx7vxDn=zGJ4J_Wp6AH?KdfCS=LDyucyMYE#+#zzy?vEY8NYng?SgrMw?Cyye!S=!$nAfngV2qkU2v9!BWh?2SSykAGw^a`.

##### Q8g II) you want to enforce a password policy [that] takes human behavior into consideration?

An option would be to require a non-dictionary phrase that is long but memorable enough to be remembered. E.g. `Roofcatsswimthefarthestonloudknightsandhairydemons`.

Another option could be to use a password store, which only require the user to know one password and have all the other passwords generated to be secure.

The passphrase should be changed within reasonable timeframes.


##### Q8g III) Why is the human factor a limitation when it comes to password securiy?

Human behavior makes it difficult to remember long random letter-number-symbol combinations. More so when one cannot re-use passwords.


### Q9) Would you consider this attack a violation of `Confidentiality`, `Integrity` and/or `Availability` and why/why not?

**Confidentiality**  
It can be considered to be a violation of `confidentiality` since unauthorized persons gain access to restricted information.

**Integrity**  
Original data has not been changed or tampered with (only read) and so the `integrity` of the data has not been breached.

**Availability**  
Access to data has not been compromised during the attack. 
